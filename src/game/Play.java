/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.Timer;
import javax.swing.*;
import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;





public class Play extends JFrame implements KeyListener{
    int rozmiar1 = 19*40;
    int rozmiar2 = 15*40;
    static JFrame jframe;
    Map newmap = new Map();
    Timer timer = new Timer();
    BufferedImage bufor;
    Graphics bg;
    BufferedImage brick;
    BufferedImage ground;
    BufferedImage faceup;
    BufferedImage facedown;
    BufferedImage faceleft;
    BufferedImage faceright;
    String url = System.getProperty("user.dir");
     
    public Play(){
        init();
        setSize(760,600);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);   
    }

    public void init(){
            try {
            addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                Menu logout = new Menu();
                logout.logoutt();
                logout.outgame();
                e.getWindow().dispose();
            }
            }); 
            jframe = this;
            jframe.addKeyListener(this);
            jframe.setSize(rozmiar1, rozmiar2);
            jframe.setBackground(Color.LIGHT_GRAY);
            bufor = (BufferedImage) createImage(rozmiar1, rozmiar2);                  
            timer.scheduleAtFixedRate(newmap, 10, 10);
            brick = ImageIO.read(new File(url + "/src/game/pictures/brick.png"));      
            ground = ImageIO.read(new File(url + "/src/game/pictures/ground.png"));
            faceup = ImageIO.read(new File(url + "/src/game/pictures/gora.png"));
            facedown = ImageIO.read(new File(url + "/src/game/pictures/dol.png"));
            faceright = ImageIO.read(new File(url + "/src/game/pictures/prawo.png"));
            faceleft = ImageIO.read(new File(url + "/src/game/pictures/lewo.png"));
            newmap.makeMap();
        } catch (IOException ex) {
            Logger.getLogger(Play.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void rysujPlansze(Graphics g){
        for (int i = 0; i < newmap.plansza.length; i++){
            for(int j = 0; j < newmap.plansza[0].length; j++){
                switch(newmap.plansza[i][j]){
                    case 0:
                        g.drawImage(ground, j*40, i*40, this);
                        break;
                    case 1:
                        g.drawImage(brick, j*40, i*40, this);
                        break;
                    default:
                    switch (Map.strona) {
                        case 'l':
                            g.drawImage(faceleft, j*40, i*40, this);
                            break;
                        case 'p':
                            g.drawImage(faceright, j*40, i*40, this);
                            break;
                        case 'g':
                            g.drawImage(faceup, j*40, i*40, this);
                            break;
                        case 'd':
                            g.drawImage(facedown, j*40, i*40, this);
                            break;
                        default:
                            g.drawImage(facedown, j*40, i*40, this);
                            break;
                    }
                        break;
                }
            }
        }
    }


    @Override
    public void update(Graphics g){
        bg.clearRect(0, 0, rozmiar1, rozmiar2);
        paint(bg);
        g.drawImage(bufor, 0, 0, this);
    }
    
    @Override
    public void paint(Graphics g){
        try {
            newmap.makeMap();
            rysujPlansze(g); 
            TimeUnit.MILLISECONDS.sleep(70);
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null,"Utracono połączenie z serwerem!");
            dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()){
            case 65:
                newmap.move('l');
                break;
            case 68:
                newmap.move('p');
                break;
            case 87:
                newmap.move('g');
                break;
            case 83:
                newmap.move('d');
                break;
            case 27:
                ImageIcon image =  new ImageIcon(url + "/src/game/pictures/Menu.png");
                JFrame frame = new JFrame();
                frame.setLayout(new GridLayout(6, 1, 5, 5));
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.add(new JLabel(" "));
                frame.add(new JLabel(image));
                frame.add(new JLabel(" "));
                JButton exit = new JButton("Wyjscie");
                
                exit.addActionListener((ActionEvent e1) -> {
                    Menu okno = new Menu();
                    okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    okno.setVisible(true);
                    okno.outgame();
                    frame.dispose();
                    dispose();
                });  
                JButton back = new JButton("Powrot");
                back.addActionListener((ActionEvent e1) -> {
                    frame.dispose();
                }); 
                JButton help = new JButton("Pomoc");
                help.addActionListener((ActionEvent e1) -> {
                    File plik = new File("pomoc.txt");
                    Scanner odczyt;
                    String pomoc = "";
                    try {
                        odczyt = new Scanner(plik);
                        while( odczyt.hasNextLine()){
                            String tekst = odczyt.nextLine() + "\n";
                            pomoc = pomoc + tekst;
                        }
                    } catch (FileNotFoundException ex) {
                        JOptionPane.showMessageDialog(null,"Nie znaleziono pliku!");
                    }
                    JOptionPane.showMessageDialog(null,pomoc);
                }); 
                frame.add(help);
                frame.add(back);
                frame.add(exit);

                frame.setSize(300, 200);
                frame.setLocationRelativeTo(null);  
                frame.setVisible(true);
                break;             
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //
    }


}
