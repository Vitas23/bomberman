/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 *
 * @author Vitas
 */
public class Info extends javax.swing.JFrame {

    /**
     * Otwiera okno, które odpowiada za wyświetlanie sterowania
     */
    public Info() {
        initComponents();
        myInitComponents();
    }
    
private void myInitComponents(){
    addWindowListener(new WindowAdapter(){
    @Override
    public void windowClosing(WindowEvent e){
        Menu logout = new Menu();
        logout.logoutt();
        e.getWindow().dispose();
        }
    }); 
    setLocationRelativeTo(null);
    pack();
    
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        controlImg = new javax.swing.JLabel();
        control2TextField = new javax.swing.JTextField();
        control3TextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        controlTextField = new javax.swing.JTextPane();
        back2Button = new javax.swing.JLabel();
        backgroundInfo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));
        setResizable(false);
        getContentPane().setLayout(null);

        controlImg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/sterowanie.png"))); // NOI18N
        getContentPane().add(controlImg);
        controlImg.setBounds(270, 250, 280, 170);

        control2TextField.setEditable(false);
        control2TextField.setFont(new java.awt.Font("High Tower Text", 0, 36)); // NOI18N
        control2TextField.setText("        Sterowanie");
        control2TextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                control2TextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(control2TextField);
        control2TextField.setBounds(250, 130, 320, 50);

        control3TextField.setEditable(false);
        control3TextField.setFont(new java.awt.Font("High Tower Text", 0, 18)); // NOI18N
        control3TextField.setText("Kontrola postaci:");
        control3TextField.setAutoscrolls(false);
        control3TextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                control3TextFieldActionPerformed(evt);
            }
        });
        getContentPane().add(control3TextField);
        control3TextField.setBounds(250, 180, 320, 60);

        controlTextField.setBackground(new java.awt.Color(83, 83, 195));
        controlTextField.setBorder(javax.swing.BorderFactory.createBevelBorder(0));
        jScrollPane1.setViewportView(controlTextField);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(250, 130, 320, 300);

        back2Button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/backButton.png"))); // NOI18N
        back2Button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                back2ButtonMousePressed(evt);
            }
        });
        getContentPane().add(back2Button);
        back2Button.setBounds(350, 470, 126, 50);

        backgroundInfo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/game/pictures/tlo.jpg"))); // NOI18N
        getContentPane().add(backgroundInfo);
        backgroundInfo.setBounds(0, 0, 800, 600);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void back2ButtonMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_back2ButtonMousePressed
        Menu mainMenu = new Menu();
        mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainMenu.setVisible(true);
        dispose();
    }//GEN-LAST:event_back2ButtonMousePressed

    private void control2TextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_control2TextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_control2TextFieldActionPerformed

    private void control3TextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_control3TextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_control3TextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel back2Button;
    private javax.swing.JLabel backgroundInfo;
    private javax.swing.JTextField control2TextField;
    private javax.swing.JTextField control3TextField;
    private javax.swing.JLabel controlImg;
    private javax.swing.JTextPane controlTextField;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
