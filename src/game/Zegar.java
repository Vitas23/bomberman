package game;


import javax.swing.*;       
import java.util.*;

class Zegar extends JFrame{


     public String timeNow(){
       Calendar now = Calendar.getInstance();
       int hrs = now.get(Calendar.HOUR_OF_DAY);
       int min = now.get(Calendar.MINUTE);
       int sec = now.get(Calendar.SECOND);
       
       String time = "Time: " + zero(hrs)+":"+zero(min)+":"+zero(sec);
       
       return time;
     }


     
     public String zero(int num){
       String number = ( num < 10) ? ("0"+num) : (""+num);
       return number;    
     }

     
}