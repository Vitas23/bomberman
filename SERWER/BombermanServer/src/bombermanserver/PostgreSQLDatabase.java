package bombermanserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jakub
 */
public class PostgreSQLDatabase {

    static int ID = -1;
    static boolean state;
    static Connection connection = null;

    public static void connect() throws SQLException {
        System.out.println("-------- PostgreSQL " + "JDBC Connection Testing ------------");
        try {
//            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
//            String url = "jdbc:ucanaccess://d:/bombermanacc.mdb";
//            //String username = "postgres";
//            //String password = "bela12";
//            connection = DriverManager.getConnection(url);
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/bomberman";
            String username = "postgres";
            String password = "bela12";
            connection = DriverManager.getConnection(url, username, password);

        } catch (ClassNotFoundException e) {
            BombermanServer.saveError(e, "Nie mozna odnalesc bazy danych!");
            System.out.println("Where is your PostgreSQL JDBC Driver? " + "Include in your library path!");
            e.printStackTrace();
            System.exit(0);
            return;
        }
        System.out.println("PostgreSQL JDBC Driver Registered!");

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    public static void connectionPostgreSQL(String user_login, String user_password) {
        PreparedStatement zapytaniePostgreSQL = null;
        int count = 0;
        String login = user_login;
        String passwd = user_password;
        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count FROM players WHERE login = '" + login + "'");
            while (rs.next()) {
                count = rs.getInt("count");
            }
            if (count == 0) {
                zapytaniePostgreSQL = connection.prepareStatement("INSERT INTO players (login, password) VALUES (?,?)");
                zapytaniePostgreSQL.setString(1, login);
                zapytaniePostgreSQL.setString(2, passwd);
                zapytaniePostgreSQL.executeUpdate();
                state = true;
            } else {
                System.out.println("Podany Login istnieje!");
            }

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

    }

    public static void connectionPostgreSQLLogin(String user_login, String user_password) {
        int count = 0;
        String login = user_login;
        String passwd = user_password;
        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count,id FROM players WHERE login = '" + login + "' and password ='" + passwd + "' group by id");
            while (rs.next()) {
                count = rs.getInt("count");
                ID = rs.getInt("id");
                System.out.println(ID);
                if (count > 0) {
                    state = true;
                }
            }
            if (count > 0) {
                stmt.executeUpdate("Update players set connected = true where id = " + ID);
            }

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

    }

    public static void addAction(String user_login, String IDe, String action) {
        PreparedStatement zapytaniePostgreSQL = null;
        int count = 0;
        String login = user_login;

        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count FROM players WHERE login = '" + login + "'");
            while (rs.next()) {
                count = rs.getInt("count");
            }
            if (count > 0) {
                zapytaniePostgreSQL = connection.prepareStatement("INSERT INTO logs (player_id, data, akcja) VALUES (?,?,?)");
                java.util.Date dt = new java.util.Date();
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String aktualnyCzas = sdf.format(dt);
                zapytaniePostgreSQL.setString(1, IDe);
                zapytaniePostgreSQL.setString(2, aktualnyCzas);
                zapytaniePostgreSQL.setString(3, action);
                zapytaniePostgreSQL.executeUpdate();
                state = true;
            } else {
                System.out.println("Podany Login istnieje!");
            }

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }
    }

    public static void connectionPostgreSQLInGame(String user_login, String user_password) {
        int count = 0;
        String login = user_login;
        String passwd = user_password;
        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count,id FROM players WHERE login = '" + login + "' and password ='" + passwd + "' group by id");
            while (rs.next()) {
                count = rs.getInt("count");
                ID = rs.getInt("id");
                System.out.println(ID);
                if (count > 0) {
                    state = true;
                }
            }
            if (count > 0) {
                stmt.executeUpdate("Update players set ingame = true where id = " + ID);
            }

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

    }

    public static void connectionPostgreSQLOutGame(String user_login, String user_password) {
        int count = 0;
        String login = user_login;
        String passwd = user_password;
        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count,id FROM players WHERE login = '" + login + "' and password ='" + passwd + "' group by id");
            while (rs.next()) {
                count = rs.getInt("count");
                ID = rs.getInt("id");
                System.out.println(ID);
                if (count > 0) {
                    state = true;
                }
            }
            stmt.executeUpdate("Update players set ingame = false where id = " + ID);

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }
    }

    public static void connectionPostgreSQLLogout(String user_login, String user_password) {
        int count = 0;
        String login = user_login;
        String passwd = user_password;
        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count,id FROM players WHERE login = '" + login + "' and password ='" + passwd + "' group by id");
            while (rs.next()) {
                count = rs.getInt("count");
                ID = rs.getInt("id");
                System.out.println(ID);
                if (count > 0) {
                    state = true;
                }
            }
            stmt.executeUpdate("Update players set connected = false where id = " + ID);

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

    }
    
        public static void logoutAll() {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("Update players set connected = false");
            stmt.executeUpdate("Update players set ingame = false");
        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

    }

    public static String viewScore() {
        String ranking = "";
        int i = 0;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select players.login as login, score.score as score from players join score on players.id = score.player_id order by score desc");
            while (rs.next()) {
                if (i > 0) {
                    ranking = ranking + ";" + rs.getString("login") + ":" + rs.getString("score");
                } else {
                    ranking = rs.getString("login") + ":" + rs.getString("score");
                    i++;
                }
            }
            return ranking;
        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return "Brak";
        }

    }

    public static String connectionCheck(String user_login, String user_password) {
        int count = 0;
        String login = user_login;
        String passwd = user_password;
        state = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) as count,id FROM players WHERE login = '" + login + "' and password ='" + passwd + "' and connected = true group by id");
            while (rs.next()) {
                count = rs.getInt("count");
                ID = rs.getInt("id");
                System.out.println(ID);
                if (count > 0) {
                    state = true;
                    return "Uzytkownik jest zalogowany";
                }
            }

        } catch (SQLException e) {
            BombermanServer.saveError(e, " ");
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return "Operacja nie powiodla sie!";
        }
        return "Login";
    }

    public static boolean getState() {
        return state;
    }

    public static int getID() {
        return ID;
    }
}
