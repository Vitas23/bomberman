package bombermanserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class BombermanServer implements Runnable {
    static int iloscG = 0;
    static String map = null;
    static int IDPlayer = 0;
    static int ID;
    Socket csocket;
    BombermanServer(Socket csocket) {
      this.csocket = csocket;

   }

public static void saveError(Exception ex, String blad){
    try {
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");
        String aktualnyCzas = sdf.format(dt);
        FileWriter file = new FileWriter("errorsserver.txt", true);
        BufferedWriter plik = new BufferedWriter(file);
        plik.write(aktualnyCzas + ": " + ex  + ", " + blad);
        plik.newLine();
        plik.close();
    } catch (IOException ex1) {
        System.out.print("Nie udalo sie zapisac do pliku! ERROR: " + ex1);
    }
}

    public static void main(String[] args) throws IOException {
        ServerSocket listener = new ServerSocket(9090);
 
        try {
            PostgreSQLDatabase.connect();
        } catch (SQLException ex) {
            saveError(ex, "Nie mozna połączyć z baza danych!");
            }
        PostgreSQLDatabase.logoutAll();
        InetAddress IP = InetAddress.getLocalHost();
        JOptionPane.showMessageDialog(null, "Host IP: " + IP.toString());

        try {
            while (true) {
                Socket socket = listener.accept();
                new Thread(new BombermanServer(socket)).start();
            }
        }
        finally {
            listener.close();
        }
        
    }

    private static String perfromAction(String[] exploded) {
        String action = exploded[0];
        String user_login = exploded[1];
        String user_password = exploded[2];
        String ranking;
        int x, y;

        switch(action){
            case "register": 
                PostgreSQLDatabase.connectionPostgreSQL(user_login, user_password);                
                if(PostgreSQLDatabase.getState())
                    return "Success";
                else
                    return "Failed";
            case "login":
                PostgreSQLDatabase.connectionPostgreSQLLogin(user_login, user_password);
                if(PostgreSQLDatabase.getState()){
                    ID = PostgreSQLDatabase.getID();
                    return "Login Succesfull;" + ID;   
                }
                else
                    return "Login Failed";
            case "connect":                
                return "Polaczono z serwerem";
            case "ranking":                
                ranking = PostgreSQLDatabase.viewScore();                
                return ranking;
            case "logout":             
                PostgreSQLDatabase.connectionPostgreSQLLogout(user_login, user_password);
                if(PostgreSQLDatabase.getState())
                    return "Logout Succesfull";
                else
                    return "Logout Failed";
            case "ingame":               
                PostgreSQLDatabase.connectionPostgreSQLInGame(user_login, user_password);
                if(PostgreSQLDatabase.getState())
                    return "Creating Game Succesfull";
                else
                    return "Creating Game Failed";
            case "outgame":
                Move.exitt(Integer.parseInt(exploded[3]));
                PostgreSQLDatabase.connectionPostgreSQLOutGame(user_login, user_password);
                iloscG--;
                if(PostgreSQLDatabase.getState())
                    return "Closing Game Succesfull";
                else
                    return "Closing Game Failed";
            case "wspolrzedne":
                x = Integer.parseInt(exploded[1]);
                y = Integer.parseInt(exploded[2]);
                IDPlayer = Integer.parseInt(exploded[3]);
                map = Move.wysylanie(x, y, IDPlayer);
                System.out.println(map);
                return map;           
            case "connectioncheck":
                String conn;
                conn = PostgreSQLDatabase.connectionCheck(user_login, user_password);
                return conn;
            case "akcja":
                String akcja;
                akcja = exploded[3];
                PostgreSQLDatabase.addAction(user_login,user_password,akcja);
                return akcja;
            case "checkplayer":
                if(iloscG == 0){
                    iloscG = 1;
                    return "Brak";
                }
                else if(iloscG == 1){
                    iloscG = 2;
                    return "Jeden";
                }
                else return "Blad";
               
                
        }
        
        return "Action not found";
    }

    @Override
    public void run() {
        try {
            PrintWriter out =
                new PrintWriter(csocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(
                new InputStreamReader(csocket.getInputStream()));
                     
            String request = in.readLine();
            String[] exploded = request.split(":");
                    
            String response = perfromAction(exploded);
            System.out.println(response);
            out.println(response);
            } catch (IOException ex) {
            saveError(ex, "Nie mozna odeslac danych!");
        } finally {
            try {
                csocket.close();
            } catch (IOException ex) {
                saveError(ex, "Socet jest zajety!");
            }
        }
    }

    
}